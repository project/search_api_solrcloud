<?php

namespace Drupal\search_api_solrcloud\Plugin\SolrCloudConnector;

use Drupal\Core\Form\FormStateInterface;
use Drupal\search_api_solrcloud\SolrCloudConnector\SolrCloudConnectorPluginBase;

/**
 * Standard SolrCloud connector.
 *
 * @SolrCloudConnector(
 *   id = "standard",
 *   label = @Translation("SolrCloud Standard"),
 *   description = @Translation("A connector which just uses SolrCloud to get information about the cluster.")
 * )
 */
class StandardSolrCloudConnector extends SolrCloudConnectorPluginBase {

    public function buildConfigurationForm(array $form, FormStateInterface $form_state)
    {
        parent::buildConfigurationForm($form, $form_state);

        $form['solrurls'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Solr URLs'),
            '#description' => $this->t('A comma seperated list of Solr urls <code>http://localhost:8983/solr</code> or <code>http://host1:8983/solr,http://host2:8983/solr</code>.'),
            '#default_value' => $this->configuration['solrurls'] ?? '',
            '#required' => TRUE,
        ];

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function defaultConfiguration() {
        $config = parent::defaultConfiguration();
        $config = array_merge($config, [
            'solrhosts' => '',
        ]);
        return $config;
    }

}
