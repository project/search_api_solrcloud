<?php

namespace Drupal\search_api_solrcloud\Plugin\SolrCloudConnector;

use Drupal\Core\Form\FormStateInterface;
use Drupal\search_api_solrcloud\SolrCloudConnector\SolrCloudConnectorPluginBase;

/**
 * Zookeeper SolrCloud connector.
 *
 * @SolrCloudConnector(
 *   id = "zookeeper",
 *   label = @Translation("SolrCloud Zookeeper"),
 *   description = @Translation("A SolrCloud connector which uses Zookeeper to get cluster information.")
 * )
 */
class ZookeeperSolrCloudConnector extends SolrCloudConnectorPluginBase {

    public function buildConfigurationForm(array $form, FormStateInterface $form_state)
    {
        parent::buildConfigurationForm($form, $form_state);

        $form['zkhosts'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Zookeeper hosts'),
            '#description' => $this->t('A comma seperated list of Zookeeper hosts <code>localhost:2181</code> or <code>localhost:2181,otherhost:2181</code>.'),
            '#default_value' => $this->configuration['zkhosts'] ?? '',
            '#required' => TRUE,
        ];

        $form['zktimeout'] = [
            '#type' => 'number',
            '#min' => 0,
            '#title' => $this->t('Zookeeper timeout'),
            '#description' => $this->t('The timeout in milliseconds for Zookeeper connections. The default is 10000 ms (10 seconds).'),
            '#default_value' => $this->configuration['zktimeout'] ?? 10000,
            '#required' => TRUE,
        ];

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function defaultConfiguration() {
        $config = parent::defaultConfiguration();
        $config = array_merge($config, [
            'zkhosts' => '',
            'zktimeout' => 10000,
        ]);
        return $config;
    }

}
