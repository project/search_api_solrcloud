<?php

namespace Drupal\search_api_solrcloud\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a connector plugin annotation object.
 *
 * Condition plugins provide generalized conditions for use in other
 * operations, such as conditional block placement.
 *
 * Plugin Namespace: Plugin\SolrCloudConnector
 *
 * @see \Drupal\search_api_solrcloud\SolrCloudConnector\SolrCloudConnectorManager
 * @see \Drupal\search_api_solrcloud\SolrCloudConnector\SolrCloudConnectorInterface
 * @see \Drupal\search_api_solrcloud\SolrCloudConnector\SolrCloudConnectorPluginBase
 *
 * @ingroup plugin_api
 *
 * @Annotation
 */
class SolrCloudConnector extends Plugin {

  /**
   * The Solr connector plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the Solr connector.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The backend description.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

}
