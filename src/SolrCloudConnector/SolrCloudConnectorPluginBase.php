<?php

namespace Drupal\search_api_solrcloud\SolrCloudConnector;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\search_api\Plugin\ConfigurablePluginBase;
use Drupal\search_api\Plugin\PluginFormTrait;
use Drupal\search_api_solrcloud\SolrCloudConnectorInterface;
use Drupal\search_api_solrcloud\SearchApiSolrCloudException;
use Solarium\Cloud\Client;
use Solarium\Cloud\Core\Client\CollectionEndpoint;
use Solarium\Core\Client\Request;
use Solarium\Core\Client\Response;
use Solarium\Core\Query\Helper;
use Solarium\Core\Query\QueryInterface;
use Solarium\Exception\HttpException;
use Solarium\QueryType\Select\Query\Query;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a base class for SolrCloud connector plugins.
 *
 * Plugins extending this class need to define a plugin definition array through
 * annotation. These definition arrays may be altered through
 * hook_search_api_solrcloud_connector_info_alter(). The definition includes the
 * following keys:
 * - id: The unique, system-wide identifier of the backend class.
 * - label: The human-readable name of the backend class, translated.
 * - description: A human-readable description for the backend class,
 *   translated.
 *
 * A complete plugin definition should be written as in this example:
 *
 * @code
 * @SolrCloudConnector(
 *   id = "my_connector",
 *   label = @Translation("My connector"),
 *   description = @Translation("Authenticates with SuperAuth™.")
 * )
 * @endcode
 *
 * @see \Drupal\search_api_solrcloud\Annotation\SolrCloudConnector
 * @see \Drupal\search_api_solrcloud\SolrCloudConnector\SolrCloudConnectorPluginManager
 * @see \Drupal\search_api_solrcloud\SolrCloudConnectorInterface
 * @see plugin_api
 */
abstract class SolrCloudConnectorPluginBase extends ConfigurablePluginBase implements SolrCloudConnectorInterface, PluginFormInterface {

  use PluginFormTrait {
    submitConfigurationForm as traitSubmitConfigurationForm;
  }

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * A connection to the SolrCloud servers.
   *
   * @var \Solarium\Cloud\Client
   */
  protected $solr;

  /**
   * {@inheritdoc}
   * TODO where is this used?
   *
   * @throws \Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException
   * @throws \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $plugin = parent::create($container, $configuration, $plugin_id, $plugin_definition);

    $plugin->eventDispatcher = $container->get('event_dispatcher');

    return $plugin;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'defaultcollection' => '',
      'querytimeout' => 5000,
      'indextimeout' => 5000,
      'updatetimeout' => 5000,
      'optimizetimeout' => 10000,
      'method' => 'AUTO',
      'skip_schema_check' => 1,
      'solr_version' => '',
    ];
  }

  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    $form['defaultcollection'] = [
      '#type' => 'textfield',
      '#title' => $this->t('SolrCloud default collection'),
      '#description' => $this->t('The name that identifies the default SolrCloud collection to use.'),
      '#default_value' => $this->configuration['defaultcollection'] ?? '',
      '#required' => TRUE,
    ];

    $form['querytimeout'] = [
      '#type' => 'number',
      '#min' => 0,
      '#title' => $this->t('Query timeout'),
      '#description' => $this->t('The timeout in milliseconds for search queries sent to the Solr servers.'),
      '#default_value' => $this->configuration['querytimeout'] ?? 5000,
      '#required' => TRUE,
    ];

    $form['indextimeout'] = [
      '#type' => 'number',
      '#min' => 1,
      '#max' => 180,
      '#title' => $this->t('Index timeout'),
      '#description' => $this->t('The timeout in milliseconds for indexing requests to the Solr server.'),
      '#default_value' => isset($this->configuration['indextimeout']) ?? 5000,
      '#required' => TRUE,
    ];

    $form['updatetimeout'] = [
      '#type' => 'number',
      '#min' => 0,
      '#title' => $this->t('Index timeout'),
      '#description' => $this->t('The timeout in seconds for index/update requests to the Solr server.'),
      '#default_value' => $this->configuration['updatetimeout'] ?? 5000,
      '#required' => TRUE,
    ];

    $form['optimizetimeout'] = [
      '#type' => 'number',
      '#min' => 0,
      '#title' => $this->t('Optimize timeout'),
      '#description' => $this->t('The timeout in seconds for background index optimization queries on a Solr server.'),
      '#default_value' => $this->configuration['optimizetimeout'] ?? 10000,
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   * @throws \Drupal\search_api_solrcloud\SearchApiSolrCloudException
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if (!empty($values['defaultcollection']) && strpos($values['defaultcollection'], '/') === 0) {
      $form_state->setError($form['defaultcollection'], $this->t('The collection must not start with "/".'));
    }

    // TODO shouldn't we check something?
    try {
      if (!$form_state->hasAnyErrors()) {
        // Try to orchestrate a server link from form values.
        $this->solr = new Client($values);
        $this->solr->setCollection($values['defaultcollection']);
      }
    }
    catch (\Exception $e) {
      throw new SearchApiSolrCloudException(t('Solr HTTP connection error.'), $e->getCode(), $e);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    // TODO never used
    $values = $form_state->getValues();

    $this->traitSubmitConfigurationForm($form, $form_state);
  }

  /**
   * Prepares the connection to the Solr server.
   *
   * @throws \Drupal\search_api_solrcloud\SearchApiSolrCloudException
   */
  protected function connect() {
    try{
      if (!$this->solr) {
        $this->solr = new Client($this->configuration);
        $this->solr->setCollection($this->getCollection());
      }
    }
    catch (\Exception $e) {
      throw new SearchApiSolrCloudException(t('Solr connection error.'), $e->getCode(), $e);
    }
  }

  /**
   * @inheritdoc
   */
  public function getOption(string $option)
  {
    if (isset($this->configuration[$option]) && !empty($this->configuration[$option])) {
      return $this->configuration[$option];
    }

    return false;
  }

  /**
   * @inheritdoc
   */
  public function getCollection()
  {
    return $this->getOption('collection') === false ? $this->getOption('defaultcollection') : $this->getOption('collection');
  }

  /**
   * Returns the Solr server URI.
   *
   * @return string|bool
   */
  public function getServerUri() {
    $endpoint = $this->getEndpoint($this->getCollection());
    if ($endpoint) {
      return $endpoint->getServerUri();
    }

    return FALSE;
  }

  /**
   * Returns the collection URI.
   *
   * @return string|bool
   */
  public function getCollectionUri() {
    $endpoint = $this->getEndpoint($this->getCollection());
    if ($endpoint) {
      return $endpoint->getBaseUri();
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getSolrVersion($force_auto_detect = FALSE) {
    // Allow for overrides by the user.
    if (!$force_auto_detect && !empty($this->configuration['solr_version'])) {
      // In most cases the already stored solr_version is just the major version
      // number as integer. In this case we will expand it to the minimum
      // corresponding full version string.
      $min_version = ['0', '0', '0'];
      $version = explode('.', $this->configuration['solr_version']) + $min_version;

      return implode('.', $version);
    }

    $info = $this->getCollectionInfo();

      // Get our solr version number.
    if (isset($info['lucene']['solr-spec-version'])) {
      return $info['lucene']['solr-spec-version'];
    }

    return '0.0.0';
  }

  /**
   * {@inheritdoc}
   */
  public function getSolrMajorVersion($version = '') {
    list($major, ,) = explode('.', $version ?: $this->getSolrVersion());
    return $major;
  }

  /**
   * {@inheritdoc}
   */
  public function getSolrBranch($version = '') {
    return $this->getSolrMajorVersion($version) . '.x';
  }

  /**
   * {@inheritdoc}
   */
  public function getLuceneMatchVersion($version = '') {
    list($major, $minor,) = explode('.', $version ?: $this->getSolrVersion());
    return $major . '.' . $minor;
  }

  /**
   * {@inheritdoc}
   */
  public function getServerInfo($reset = FALSE) {
    // TODO fix this to a correct QueryType.
    return $this->getDataFromHandler('admin/info/system', $reset);
  }

  /**
   * {@inheritdoc}
   */
  public function getCollectionInfo($reset = FALSE) {
    return $this->getDataFromHandler('admin/system', $reset);
  }

  /**
   * {@inheritdoc}
   */
  public function getLuke() {
    return $this->getDataFromHandler('admin/luke', TRUE);
  }

  /**
   * {@inheritdoc}
   * @throws \Drupal\search_api_solrcloud\SearchApiSolrCloudException
   */
  public function getSchemaVersionString($reset = FALSE) {
    return $this->getCollectionInfo($reset)['core']['schema'];
  }

  /**
   * {@inheritdoc}
   * @throws \Drupal\search_api_solrcloud\SearchApiSolrCloudException
   */
  public function getSchemaVersion($reset = FALSE) {
    $parts = explode('-', $this->getSchemaVersionString($reset));
    return $parts[1];
  }

  /**
   * Gets data from a Solr endpoint using a given handler.
   *
   * @param string $handler
   * @param bool $reset
   *   If TRUE the server will be asked regardless if a previous call is cached.
   *
   * @return array
   *   A response array with system information.
   *
   * @throws \Drupal\search_api_solrcloud\SearchApiSolrCloudException
   */
  protected function getDataFromHandler($handler, $reset = FALSE) {
    // TODO Refactor $handler, better to use QueryTypes
    static $previous_calls = [];
    $endpoint_url = 'solr'; // TODO refactor, is only used for previous_calls
    $this->connect();

    $state_key = 'search_api_solrcloud.endpoint.data';
    $state = \Drupal::state();
    $endpoint_data = $state->get($state_key);

    if ($reset || !isset($previous_calls[$endpoint_url][$handler])) {

      if ($reset || !is_array($endpoint_data) || !isset($endpoint_data[$endpoint_url][$handler])) {


        try {
          $query = $this->solr->createPing(['handler' => $handler]);
          $endpoint_data[$endpoint_url][$handler] = $this->solr->execute($query)->getData();
        }
        catch (HttpException $e) {
          throw new SearchApiSolrCloudException(t('Solr endpoint @endpoint not found.', ['@endpoint' => $endpoint_url]), $e->getCode(), $e);
        }

        $state->set($state_key, $endpoint_data);
      }
    }

    return $endpoint_data[$endpoint_url][$handler];
  }

  /**
   * {@inheritdoc}
   */
  public function pingCollection() {
      return $this->doPing();
  }

  /**
   * {@inheritdoc}
   */
  public function pingServer() {
    //return $this->doPing(['handler' => 'admin/info/system'], 'server');
  }

  /**
   * Pings the Solr server to tell whether it can be accessed.
   *
   * @param string $endpoint_name
   *   The endpoint to be pinged on the Solr server.
   *
   * @return mixed
   *   The latency in milliseconds if the collection can be accessed,
   *   otherwise FALSE.
   * @throws \Drupal\search_api_solrcloud\SearchApiSolrCloudException
   */
  protected function doPing($options = []) {
    $this->connect();
    // Default is ['handler' => 'admin/ping'].

    try {
      if ($this->solr) {
        $query = $this->solr->createPing($options);
        $start = microtime(TRUE);
        $result = $this->solr->execute($query);
        if ($result->getResponse()->getStatusCode() == 200) {
          // Add 1 µs to the ping time so we never return 0.
          return (microtime(TRUE) - $start) + 1E-6;
        }
      }
    }
    catch (HttpException $e) {
      throw new SearchApiSolrCloudException(t('Solr HTTP connection error.'), $e->getCode(), $e);
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatsSummary() {
    $this->connect();

    $summary = [
      '@pending_docs' => '',
      '@autocommit_time_seconds' => '',
      '@autocommit_time' => '',
      '@deletes_by_id' => '',
      '@deletes_by_query' => '',
      '@deletes_total' => '',
      '@schema_version' => '',
      '@collection_name' => '',
      '@index_size' => '',
    ];

    $query = $this->solr->createPing();
    $query->setResponseWriter(Query::WT_PHPS);
    $query->setHandler('admin/mbeans?stats=true');
    try {
      $stats = $this->solr->execute($query)->getData();
      if (!empty($stats)) {
        $update_handler_stats = $stats['solr-mbeans']['UPDATEHANDLER']['updateHandler']['stats'];
        $summary['@pending_docs'] = (int) $update_handler_stats['docsPending'];
        $max_time = (int) $update_handler_stats['autocommit maxTime'];
        // Convert to seconds.
        $summary['@autocommit_time'] = \Drupal::service('date.formatter')->formatInterval($max_time / 1000);
        $summary['@deletes_by_id'] = (int) $update_handler_stats['deletesById'];
        $summary['@deletes_by_query'] = (int) $update_handler_stats['deletesByQuery'];
        $summary['@deletes_total'] = $summary['@deletes_by_id'] + $summary['@deletes_by_query'];
        $summary['@schema_version'] = $this->getSchemaVersionString(TRUE);
        $summary['@core_name'] = $stats['solr-mbeans']['CORE']['core']['stats']['coreName'];
        // Index size variable defers with SOLR version.
        if (isset($stats['solr-mbeans']['QUERYHANDLER']['/replication']['stats']['indexSize'])) {
          $summary['@index_size'] = $stats['solr-mbeans']['QUERYHANDLER']['/replication']['stats']['indexSize'];
        }
        elseif (isset($stats['solr-mbeans']['CORE']['core']['stats']['size'])) {
          $summary['@index_size'] = $stats['solr-mbeans']['CORE']['core']['stats']['size'];
        }
      }
      return $summary;
    }
    catch (HttpException $e) {
      throw new SearchApiSolrCloudException(t('Solr server collection @collection not found.', ['@collection' => $this->getEndpoint($this->getCollection())->getBaseUri()]), $e->getCode(), $e);
    }
  }

  /**
   * {@inheritdoc}
   * @throws \Drupal\search_api_solrcloud\SearchApiSolrCloudException
   */
  public function collectionRestGet($path) {
    return $this->restRequest('collection', $path);
  }

  /**
   * {@inheritdoc}
   * @throws \Drupal\search_api_solrcloud\SearchApiSolrCloudException
   */
  public function collectionRestPost($path, $command_json = '') {
    return $this->restRequest('collection', $path, Request::METHOD_POST, $command_json);
  }

  /**
   * {@inheritdoc}
   * @throws \Drupal\search_api_solrcloud\SearchApiSolrCloudException
   */
  public function serverRestGet($path) {
    return $this->restRequest('server', $path);
  }

  /**
   * {@inheritdoc}
   * @throws \Drupal\search_api_solrcloud\SearchApiSolrCloudException
   */
  public function serverRestPost($path, $command_json = '') {
    return $this->restRequest('server', $path, Request::METHOD_POST, $command_json);
  }

  /**
   * Sends a REST request to the Solr server endpoint and returns the result.
   *
   * @param string $endpoint_key
   *   The endpoint that refelcts the base URL.
   * @param string $path
   *   The path to append to the base URL.
   * @param string $method
   *   The HTTP request method.
   * @param string $command_json
   *   The command to send encoded as JSON.
   *
   * @return string
   * @throws \Drupal\search_api_solrcloud\SearchApiSolrCloudException
   *   The decoded response.
   */
  protected function restRequest($endpoint_key, $path, $method = Request::METHOD_GET, $command_json = '') {
    $this->connect();

    $request = new Request();
    $request->setMethod($method);
    $request->addHeader('Accept: application/json');
    if (Request::METHOD_POST == $method) {
      $request->addHeader('Content-type: application/json');
      $request->setRawData($command_json);
    }
    $request->setHandler($path);

    $endpoint = $this->getEndpoint($this->getCollection());
    $timeout = $endpoint->getTimeout();
    // @todo Destinguish between different flavors of REST requests and use
    //   different timeout settings.
    $endpoint->setTimeout($this->configuration['optimizetimeout']);
    $response = $this->solr->executeRequest($request, $endpoint);
    $endpoint->setTimeout($timeout);
    $output = Json::decode($response->getBody());
    // \Drupal::logger('search_api_solrcloud')->info(print_r($output, true));
    if (!empty($output['errors'])) {
      throw new SearchApiSolrCloudException('Error trying to send a REST request.' .
        "\nError message(s):" . print_r($output['errors'], TRUE));
    }
    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function getUpdateQuery() {
    $this->connect();
    return $this->solr->createUpdate();
  }

  /**
   * {@inheritdoc}
   */
  public function getSelectQuery() {
    $this->connect();
    return $this->solr->createSelect();
  }

  /**
   * {@inheritdoc}
   */
  public function getMoreLikeThisQuery() {
    $this->connect();
    return $this->solr->createMoreLikeThis();
  }

  /**
   * {@inheritdoc}
   */
  public function getTermsQuery() {
    $this->connect();
    return $this->solr->createTerms();
  }

  /**
   * {@inheritdoc}
   */
  public function getQueryHelper(QueryInterface $query = NULL) {
    if ($query) {
      return $query->getHelper();
    }

    return new Helper();
  }

  /**
   * {@inheritdoc}
   */
  public function getExtractQuery() {
    $this->connect();
    return $this->solr->createExtract();
  }

  /**
   * @return \Solarium\Plugin\CustomizeRequest\CustomizeRequest
   */
  protected function customizeRequest() {
    $this->connect();
    return $this->solr->getPlugin('customizerequest');
  }

  /**
   * {@inheritdoc}
   */
  public function search(\Solarium\QueryType\Select\Query\Query $query, CollectionEndpoint $endpoint = NULL) {
    $this->connect();

    if (!$endpoint) {
      $endpoint = $this->getEndpoint($this->getCollection());
    }

    // Use the 'postbigrequest' plugin if no specific http method is
    // configured. The plugin needs to be loaded before the request is
    // created.
    if ($this->configuration['method'] == 'AUTO') {
      $this->solr->getPlugin('postbigrequest');
    }

    // Use the manual method of creating a Solarium request so we can control
    // the HTTP method.
    $request = $this->solr->createRequest($query);

    // Set the configured HTTP method.
    if ($this->configuration['method'] == 'POST') {
      $request->setMethod(Request::METHOD_POST);
    }
    elseif ($this->configuration['method'] == 'GET') {
      $request->setMethod(Request::METHOD_GET);
    }

    return $this->solr->executeRequest($request, $endpoint);
  }

  /**
   * {@inheritdoc}
   */
  public function createSearchResult(\Solarium\QueryType\Select\Query\Query $query, Response $response) {
    return $this->solr->createResult($query, $response);
  }

  /**
   * {@inheritdoc}
   */
  public function update(\Solarium\QueryType\Update\Query\Query $query) {
    $this->connect();

    $endpoint = $this->getEndpoint($this->getCollection());

    // The default timeout is set for search queries. The configured timeout
    // might differ and needs to be set now because solarium doesn't
    // distinguish between these types.
    $timeout = $endpoint->getTimeout();
    $endpoint->setTimeout($this->configuration['updatetimeout']);

    $result = $this->solr->execute($query);

    // Reset the timeout setting to the default value for search queries.
    $endpoint->setTimeout($timeout);

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function execute(QueryInterface $query, CollectionEndpoint $endpoint = NULL) {
    $this->connect();

    return $this->solr->execute($query);
  }

  /**
   * {@inheritdoc}
   */
  public function optimize(string $collection = null) {
    $this->connect();

    $endpoint = $this->getEndpoint($this->getCollection());

    // The default timeout is set for search queries. The configured timeout
    // might differ and needs to be set now because solarium doesn't
    // distinguish between these types.
    $timeout = $endpoint->getTimeout();
    $endpoint->setTimeout($this->configuration['optimizetimeout']);

    $update_query = $this->solr->createUpdate();
    $update_query->addOptimize(TRUE, FALSE);

    $this->solr->execute($update_query);

    // Reset the timeout setting to the default value for search queries.
    $endpoint->setTimeout($timeout);
  }

  /**
   * {@inheritdoc}
   */
  public function extract(QueryInterface $query) {
    $this->connect();
    return $this->solr->extract($query);
  }

  /**
   * {@inheritdoc}
   */
  public function getContentFromExtractResult(ExtractResult $result, $filepath){
    $response = $result->getResponse();
    $json_data = $response->getBody();
    $array_data = Json::decode($json_data);
    return $array_data[$filepath];
  }

  /**
   * {@inheritdoc}
   */
  public function getEndpoint(string $collection) {
      $this->connect();
      $endpoint = $this->solr->getEndpoint($collection);
      if (is_object($endpoint)) {
        return $endpoint;
      }

      return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getFile($file = NULL) {
    $this->connect();

    $query = $this->solr->createPing();
    $query->setHandler('admin/file');
    $query->addParam('contentType', 'text/xml;charset=utf-8');
    if ($file) {
      $query->addParam('file', $file);
    }

    try {
      return $this->solr->execute($query)->getResponse();
    }
    catch (HttpException $e) {
      throw new SearchApiSolrCloudException($this->t('Solr server colleciton @collection not found.', ['@collection' => $this->getEndpoint()->getBaseUri()]), $e->getCode(), $e);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function viewSettings() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function __sleep() {
    // It's safe to unset the solr client completely before serialization
    // because connect() will set it up again correctly after deserialization.
    unset($this->solr);
    return parent::__sleep();
  }

}
