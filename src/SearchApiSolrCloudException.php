<?php

namespace Drupal\search_api_solrcloud;

use Drupal\search_api\SearchApiException;

/**
 * Represents an exception that occurs in Search API SolrCloud.
 */
class SearchApiSolrCloudException extends SearchApiException {}
